<?php
/**
 * Created by PhpStorm.
 * User: Tomas
 * Date: 18/12/2018
 * Time: 8:39
 */

require_once __DIR__ . '/../database/QueryBuilder.php';

class ContactRepository extends QueryBuilder
{

    public function __construct(string $table='contactos', $classEntity='Contacto')
    {
        parent::__construct($table, $classEntity);
    }

}